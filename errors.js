/* 
** sharepoint pass
* uses windows or gitlab environment variables to determine which T3 environment to push to
*/ 
try {
    if(!process.env.NWPassSP || !process.env.NWUser) {
        console.log('SharePoint Credentials do not exist, have you updated your environment variables?');
        process.exit(1);
    }
    passwordSP = Buffer.from(process.env.NWPassSP, 'base64').toString('ascii');
    userSP = process.env.NWUser; 
} catch (error) {
    console.log('Password conversion error: ', error);
    process.exit(1);
} 

/*
** hana pass
*/
try {
    if(!process.env.NWPassHana || !process.env.NWUserHana) {
        console.log('HANA Credentials do not exist, have you updated your environment variables?');
        process.exit(1);
    }
    passwordHana = Buffer.from(process.env.NWPassHana, 'base64').toString('ascii');
    userHana = process.env.NWUserHana; 
} catch (error) {
    console.log('Password conversion error: ', error);
    process.exit(1);
}

/* 
** T3 Environments
*/
try {
    if(!process.env.oEnv) {
        console.log('T3 Environment has not been set, please confirm your upload destination.');
        process.exit(1);
    }
    sourceEnvironment = process.env.oEnv;
} catch (error) {
    console.log('T3 Environment Error: ', error);
    process.exit(1);
}

module.exports = {
    passwordSP: passwordSP,
    userSP: userSP,
    passwordHana: passwordHana,
    userHana: userHana,
    sourceEnvironment: sourceEnvironment }
