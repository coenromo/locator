const httpntlm = require('httpntlm');
const uri = require('./sql.json');
const errorHandler = require('./errors.js');

var promises = [];

// deal with max $top=5000 results set by api
const getByPath = (path, obj) => {
    let paths = path.split('.');
    let officesJoin = obj;

    for (let i = 0; i < paths.length; i++) {
        if (!officesJoin[paths[i]]) {
            return;
        }
        officesJoin = officesJoin[paths[i]];
        }
        return officesJoin;
    };

    // create promise for each data element
    for (const element of uri) {
            var assetsProm = new Promise((resolve, reject) => {
                url = element.sUrl;
                var cleanData = [];
                extraResult(url, element, cleanData, resolve);
        });
        promises.push(assetsProm);
    };

    // iterate through results 
    function extraResult (url, element, cleanData, resolve) {
        httpntlm.get({
            url: url,
            username: errorHandler.userSP,
            password: errorHandler.passwordSP,
            rejectUnauthorized: false,
            headers: {
                accept: 'application/json;odata=verbose'
            }
        }, function (err, res) {
            if(err) {
                console.log('Credentials Error: ', err);
                process.exit(1);
            }

            // process and clean data
            var data = JSON.parse(res.body).d.results;
            let searchFields = element.sFields;

            for(let restData in data){
                var cleanEntry = [];
                for(let searchValue in searchFields){ //correcting type as needed
                    var sValue = data[restData][searchFields[searchValue].share];
                    //converting empty strings to null values
                    sValue = (sValue === "" ? null : sValue);
                    //type conversion for non-null entries
                    if(sValue !== null){
                        switch(searchFields[searchValue].type){
                            case "string":
                                sValue = sValue.toString();
                                break;
                            case "boolInt":
                                sValue = (sValue == true? 1 : 0);
                                break;
                            case "postcode":
                                //converting to string and padding to 4 digits if short.
                                sValue = sValue.toString().padStart(4, "0");
                                break;
                            case "hours":
                                sValue = (sValue === 0 ? null : sValue);
                                break; 
                            case "join":
                                sValue = getByPath(searchFields[searchValue].share, data[restData]);
                                break;
                            default:
                                break;
                        }
                    }
                    cleanEntry.push(sValue); 
                }
                cleanData.push(cleanEntry);
            }

            // check for __next url in response
            var responseNext = JSON.parse(res.body).d.__next;
            if (responseNext) {
                extraResult(responseNext, element, cleanData, resolve);
            } else {
                resolve({tableName:element.sHanaSQL, fields:element.sFields, data:cleanData});
            }
        });
    }

 Promise.all(promises).then((batchData) => {
    const hanaConnect = require('./hanaUpload.js');
    hanaConnect(batchData);
});
