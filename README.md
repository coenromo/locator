# Office Locator 

## Uploader App

This app pulls data from SharePoint API and posts the data to T3D HANA.

## What

The data retrieved here from SharePoint is used in Services Australia's Locator application, which is an internal search tool that staff use to find People, Places, Services and Organisations. 

## Why

Business update the hours and locations of service centres by entering in the new data into SharePoint. They change quite often in Indigenous areas due to the following:
* Cultural differences
* An Elder passing away
* Celebrations
* They also get updated in Metro areas due to relocation etc

We need the latest People, Orgs, Places and Services information to display on HANA Locator.

## Current

Busines directors provide an excel spreadsheet from various SharePoint database tables to our team. We then use a manually created script to _wipe_ the HANA tables and upload the entire excel spreadsheet data to HANA. They also only provide this excel document once a day, once a week, or sometimes only when they _think_ service centre hours have changed. 

## Improvement

* Use SharePoint API to retrieve data from database tables using RESTful API
* Use hanauploader npm package to POST data to T3 environment
* Run pipeline in GitLab every hour to get the most up to date information
