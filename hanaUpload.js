    /**
    * remove references to any SAP url's
    */
    
    const hdb = require('hdb');
    const errorHandler = require('./errors.js');
    var uploadPromises = [];

    //connect to client 
    var client = hdb.createClient({
        host: 'your' + errorHandler.sourceEnvironment + 'url-here',
        port: 33015,
        databaseName: errorHandler.sourceEnvironment,
        user: errorHandler.userHana,
        password: errorHandler.passwordHana
    });
    client.on('error', function (err) { //error handling
        console.error('Network connection error', err);
        process.exit(1);
    });
    client.connect(function (err) {
        if(err) {
        console.error('Connect error', err);
        process.exit(1);
        }
    });

    // upload batchData
    const uploadData = (batchData) => {
    for (const element of batchData) {        
        var uploadProm = new Promise((resolve, reject) => {

            //constructing sql statement
            var sStatement = 'UPSERT "SCHEMA"."database.link.'+ element.tableName +'" ( ';
            sStatement += element.fields.map(entry => '"'+entry.hana+'"').join(', ') + ', "DATA_SYNC_TIMESTAMP") VALUES ( ';
            sStatement += element.fields.map(entry => '?').join(', ') + ', CURRENT_TIMESTAMP) WITH PRIMARY KEY';


            //upsert statement batch
            client.prepare(sStatement, function(err, statement){
                if(err) {
                    console.error("Prepare error:", err);
                    process.exit(1);
                }
                statement.exec(element.data, function(err, rows){
                    if(err) {
                        console.error('Execute error:', err);
                        process.exit(1);
                    }
                    console.log('Updated ', rows.length, 'entries in ', errorHandler.sourceEnvironment, 'from ', element.tableName);

                    //delete statement
                    client.exec('DELETE FROM "SCHEMA"."database-link' + element.tableName + '" WHERE "DATA_SYNC_TIMESTAMP" < ADD_SECONDS(CURRENT_TIMESTAMP, -3600)', function(err, delrow){
                        if(err){
                            console.error('Deletion error: ', err);
                            process.exit(1);
                        }
                        console.log('Deleted ', delrow, 'entries in ', errorHandler.sourceEnvironment, 'from ', element.tableName);
                        resolve(client);
                    });

                });  
            });
        });
        uploadPromises.push(uploadProm);
    }

    Promise.all(uploadPromises).then((clients)=>{
        clients[0].end();
    });
}

module.exports = uploadData;
